from django.urls import path
from tasks.views import create_task, show_my_tasks, cross_off, uncross


urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("cross_off/<int:id>", cross_off, name="cross_off"),
    path("uncross/<int:id>", uncross, name="uncross")
]
