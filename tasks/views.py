from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


### Can't remember if this is correct or not
@login_required
def edit_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("show_my_tasks")
    else:
        form = TaskForm(id)
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "task_list": tasks,
    }
    return render(request, "tasks/my_tasks.html", context)


@login_required
def cross_off(request, id):
    item = Task.objects.get(id=id)
    item.is_completed = True
    item.save()
    return redirect(request.META['HTTP_REFERER'])


@login_required
def uncross(request, id):
    item = Task.objects.get(id=id)
    item.is_completed = False
    item.save()
    return redirect(request.META['HTTP_REFERER'])
